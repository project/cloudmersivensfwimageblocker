About this module
-----------------

This module integrates with the Cloudmersive NSFW Image Detecting API.

Uploaded files are forwarded to the Cloudmersive NSFW/Unsafe Image service and checked for NSFW/Inappropriate/Sexual/Unsafe content using advanced machine learning. 

Unsafe/inappropriate/sexual/offensive is blocked in the validation routine, so that it cannot be saved - protecting your site.


Prerequisites
-------------

This module requires a Cloudmersive NSFW API service.

Sign up for a free API key at https://cloudmersive.com - free keys include 50,000 API calls/month.


Setup
-----

- Get a Cloudmersive API key at https://cloudmersive.com

- Enable this module.

- Configure the module at /admin/config/media/cloudmersivensfw and set the API Key

- Try uploading a file